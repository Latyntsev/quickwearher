//
//  QWWeatherRequestByCoordinate.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWWeatherRequestByCoordinate.h"


@implementation QWWeatherRequestByCoordinate

@dynamic latitud;
@dynamic longitude;

- (QWWeatherRequestType)requestType
{
	return QWWeatherRequestType_byCoordinate;
}

@end
