//
//  QWWeatherForecastData.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QWWeatherData, QWWeatherRequest;

@interface QWWeatherForecastData : NSManagedObject

@property (nonatomic, retain) NSOrderedSet *forecast;
@property (nonatomic, retain) QWWeatherRequest *request;
@end

@interface QWWeatherForecastData (CoreDataGeneratedAccessors)

- (void)insertObject:(QWWeatherData *)value inForecastAtIndex:(NSUInteger)idx;
- (void)removeObjectFromForecastAtIndex:(NSUInteger)idx;
- (void)insertForecast:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeForecastAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInForecastAtIndex:(NSUInteger)idx withObject:(QWWeatherData *)value;
- (void)replaceForecastAtIndexes:(NSIndexSet *)indexes withForecast:(NSArray *)values;
- (void)addForecastObject:(QWWeatherData *)value;
- (void)removeForecastObject:(QWWeatherData *)value;
- (void)addForecast:(NSOrderedSet *)values;
- (void)removeForecast:(NSOrderedSet *)values;
@end

@interface QWWeatherForecastData (Analitycs)

- (NSNumber *)minTemperature;
- (NSNumber *)maxTemperature;
- (NSInteger)temperatureDelta;
- (NSDate *)minDate;
- (NSDate *)maxDate;
- (NSTimeInterval)dateDelta;

@end
