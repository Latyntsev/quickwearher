//
//  QWWeatherRequestByCoordinate.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "QWWeatherRequest.h"


@interface QWWeatherRequestByCoordinate : QWWeatherRequest

@property (nonatomic) double latitud;
@property (nonatomic) double longitude;

@end
