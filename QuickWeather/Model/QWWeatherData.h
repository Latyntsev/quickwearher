//
//  QWWeatherData.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QWWeatherForecastData, QWWeatherRequest;

typedef NS_ENUM(int64_t, QWWeatherType)
{
    QWWeatherType_clear,
    QWWeatherType_clouds,
    QWWeatherType_rain
};

@interface QWWeatherData : NSManagedObject

@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSNumber * temperature;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic) QWWeatherType weatherType;
@property (nonatomic, retain) QWWeatherRequest *request;
@property (nonatomic, retain) QWWeatherForecastData *forecast;

- (UIImage *)weatherIconImage;

@end
