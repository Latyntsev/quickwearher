//
//  QWWeatherRequest.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreData;

@class QWWeatherData, QWWeatherForecastData;

typedef NS_ENUM(NSInteger, QWWeatherRequestType) {
    QWWeatherRequestType_undefined = NSNotFound,
    QWWeatherRequestType_byCityName = 0,
    QWWeatherRequestType_byCoordinate = 1
};


@interface QWWeatherRequest : NSManagedObject

@property (nonatomic, retain) QWWeatherForecastData *forecastData;
@property (nonatomic, retain) QWWeatherData *weatherData;

- (QWWeatherRequestType)requestType;
+ (NSString *)name;
@end
