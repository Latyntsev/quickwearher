//
//  QWWeatherRequest.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWWeatherRequest.h"
#import "QWWeatherData.h"
#import "QWWeatherForecastData.h"


@implementation QWWeatherRequest

@dynamic forecastData;
@dynamic weatherData;

- (QWWeatherRequestType)requestType
{
	return QWWeatherRequestType_undefined;
}

+ (NSString *)name
{
    return [[self class] description];
}

@end
