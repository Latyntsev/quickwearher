//
//  QWWeatherForecastData.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWWeatherForecastData.h"
#import "QWWeatherData.h"
#import "QWWeatherRequest.h"


@implementation QWWeatherForecastData

@dynamic forecast;
@dynamic request;

@end

@implementation QWWeatherForecastData (Analitycs)

- (NSNumber *)minTemperature
{
    return [self.forecast valueForKeyPath:@"@min.temperature"];
}

- (NSNumber *)maxTemperature
{
    return [self.forecast valueForKeyPath:@"@max.temperature"];
}

- (NSInteger)temperatureDelta
{
    return self.maxTemperature.integerValue - self.minTemperature.integerValue;
}

- (NSDate *)minDate
{
    return [self.forecast valueForKeyPath:@"@min.date"];;
}

- (NSDate *)maxDate
{
    return [self.forecast valueForKeyPath:@"@max.date"];;
}

- (NSTimeInterval)dateDelta
{
    return self.maxDate.timeIntervalSince1970 - self.minDate.timeIntervalSince1970;
}

@end
