//
//  QWWeatherData.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWWeatherData.h"
#import "QWWeatherForecastData.h"
#import "QWWeatherRequest.h"


@implementation QWWeatherData

@dynamic location;
@dynamic temperature;
@dynamic date;
@dynamic weatherType;
@dynamic request;
@dynamic forecast;

- (UIImage *)weatherIconImage
{
    NSString *iconName = nil;
    switch (self.weatherType) {
        case QWWeatherType_clear:
            iconName = @"Clear.png";
            break;
        case QWWeatherType_clouds:
            iconName = @"Clouds.png";
            break;
        case QWWeatherType_rain:
            iconName = @"Rain.png";
            break;
    }
	return [UIImage imageNamed:iconName];
}

@end
