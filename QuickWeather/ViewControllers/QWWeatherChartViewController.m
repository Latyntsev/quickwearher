//
//  QWWeatherChartViewController.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWWeatherChartViewController.h"
#import "QWService.h"
#import "QWWeatherForecastData.h"
#import "QWWeatherChartView.h"
#import "QWWeatherData.h"

static NSInteger forecastDayCount = 3;

@interface QWWeatherChartViewController ()

@end

@implementation QWWeatherChartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self showProgressView];
    [[QWService sharedInstance] getWeatherForecast:self.weatherRequest forDays:forecastDayCount withHandler:^(QWWeatherForecastData *wetherForecastData, NSError *error, QWWeatherDataState state) {
        [self hideProgressView];
        [self.chartView reloadData];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - QWWeatherChartViewDataSource
- (NSInteger)weatherChartViewNumberOfPoints:(QWWeatherChartView *)weatherChartView
{
    return self.weatherRequest.forecastData.forecast.count;
}

- (CGPoint)weatherChartView:(QWWeatherChartView *)weatherChartView pointAtIndex:(NSInteger)index
{
    QWWeatherData *weatherData = self.weatherRequest.forecastData.forecast[index];
    
    CGFloat dateDelta = self.weatherRequest.forecastData.dateDelta;
    NSTimeInterval minDateTime = self.weatherRequest.forecastData.minDate.timeIntervalSince1970;
    CGFloat position = (weatherData.date.timeIntervalSince1970 -  minDateTime) / dateDelta;
    
    CGFloat temperatureDelta = (CGFloat)self.weatherRequest.forecastData.temperatureDelta;
    NSInteger minTemperature = [[self.weatherRequest.forecastData minTemperature] integerValue];
    CGFloat level = (weatherData.temperature.integerValue - minTemperature) / temperatureDelta;
    
    
    return (CGPoint){position,level};
}


- (NSString *)weatherChartView:(QWWeatherChartView *)weatherChartView labelAtType:(QWWeatherChartViewLabelType)labelType
{
    QWWeatherForecastData *forecastData = self.weatherRequest.forecastData;
    static NSDateFormatter *df;
    if (!df) {
        df = [[NSDateFormatter alloc] init];
        df.dateFormat = @"dd/MM hh:mm";
    }
    
    NSString *result = @"";
    switch (labelType) {
        case QWWeatherChartViewLabelType_minX:
            result = [NSString stringWithFormat:@"%ld°C",(long)forecastData.minTemperature.integerValue];
            break;
        case QWWeatherChartViewLabelType_maxX:
            result = [NSString stringWithFormat:@"%ld°C",(long)forecastData.maxTemperature.integerValue];
            break;
        case QWWeatherChartViewLabelType_minY:
            result = [df stringFromDate:forecastData.minDate];
            break;
        case QWWeatherChartViewLabelType_maxY:
            result = [df stringFromDate:forecastData.maxDate];
            break;
            
    }
    return result;
}


@end
