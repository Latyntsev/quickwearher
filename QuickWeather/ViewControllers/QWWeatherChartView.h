//
//  QWWeatherChartView.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <UIKit/UIKit.h>
@class QWWeatherChartView;

typedef NS_ENUM(NSInteger, QWWeatherChartViewLabelType)
{
    QWWeatherChartViewLabelType_minX,
    QWWeatherChartViewLabelType_maxX,
    QWWeatherChartViewLabelType_minY,
    QWWeatherChartViewLabelType_maxY
};

@protocol QWWeatherChartViewDataSource <NSObject>

@required
- (NSInteger)weatherChartViewNumberOfPoints:(QWWeatherChartView *)weatherChartView;
- (CGPoint)weatherChartView:(QWWeatherChartView *)weatherChartView pointAtIndex:(NSInteger)index;
- (NSString *)weatherChartView:(QWWeatherChartView *)weatherChartView labelAtType:(QWWeatherChartViewLabelType)labelType;

@end

@interface QWWeatherChartView : UIView

@property (nonatomic,weak) IBOutlet id<QWWeatherChartViewDataSource> dataSource;
- (void)reloadData;

@end
