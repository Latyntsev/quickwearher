//
//  QWProgressViewController.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWProgressViewController : UIViewController

@property (nonatomic,weak) IBOutlet UILabel *messageLabel;
+ (NSString *)identifier;

@end
