//
//  QWWeatherDetailsViewController.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <UIKit/UIKit.h>
@class QWWeatherData;
@class QWWeatherRequest;

@interface QWWeatherDetailsViewController : UIViewController

@property (nonatomic,weak) IBOutlet UIImageView *weatherIconImageView;
@property (nonatomic,weak) IBOutlet UILabel *cityNameLabel;
@property (nonatomic,weak) IBOutlet UILabel *temperatureLabel;

@property (nonatomic,strong) QWWeatherRequest *weatherRequest;

@end
