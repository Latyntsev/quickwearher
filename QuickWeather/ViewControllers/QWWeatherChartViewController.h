//
//  QWWeatherChartViewController.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <UIKit/UIKit.h>
@class QWWeatherRequest;
@class QWWeatherChartView;

@interface QWWeatherChartViewController : UIViewController

@property (nonatomic,strong) QWWeatherRequest *weatherRequest;
@property (nonatomic,weak) IBOutlet QWWeatherChartView *chartView;

@end
