//
//  QWSelectCityViewController.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWSelectCityViewController : UIViewController

@property (nonatomic,weak) IBOutlet UITextField *cityNameTextField;
@property (nonatomic,weak) IBOutlet UIButton *doneButton;

- (IBAction)onClickDone:(id)sender;
- (IBAction)onCityNameChanged:(id)sender;

@end
