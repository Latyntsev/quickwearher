//
//  QWMainViewController.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWMainViewController.h"
#import "QWLocationService.h"
#import "QWWeatherDetailsViewController.h"
#import "QWWeatherRequest.h"
#import "QWService.h"

static NSString *kWeatherDetailsSegueIdentifier = @"WeatherDeteils";

@interface QWMainViewController ()

@property (nonatomic,strong) QWLocationService *locationService;

@end

@implementation QWMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.locationService = [[QWLocationService alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickWeatherByCityName:(id)sender
{
    [self performSegueWithIdentifier:@"SelectCityName" sender:nil];
}

- (IBAction)onClickWeatherByLocation:(id)sender
{
    __block BOOL isMoved = false;
    [self showProgressView];
    [self.locationService getCurrentLocation:^(CLLocationCoordinate2D coordinate, NSError *error) {
        if (!error) {
            QWService *service = [QWService sharedInstance];
            QWWeatherRequest *request = [service requestByCoordinate:coordinate];
            [service getWeather:request withHandler:^(QWWeatherData *wetherData, NSError *error, QWWeatherDataState state) {
                [self hideProgressView];
                if (error) {
                    [[UIAlertView alertViewWithError:error] show];
                } else {
                    if (!isMoved) {
                        [self performSegueWithIdentifier:kWeatherDetailsSegueIdentifier sender:request];
                        isMoved = true;
                    }
                }
            }];
            
        } else {
            [self hideProgressView];
            [[UIAlertView alertViewWithError:error] show];
        }
    }];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kWeatherDetailsSegueIdentifier]) {
        QWWeatherDetailsViewController *vc = segue.destinationViewController;
        vc.weatherRequest = sender;
    }
}


@end
