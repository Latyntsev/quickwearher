//
//  UIViewController+ProgressView.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 25.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "UIViewController+ProgressView.h"
#import "QWProgressViewController.h"

@implementation UIViewController (ProgressView)

- (QWProgressViewController *)progressViewController
{
    static QWProgressViewController *progressViewController;
    if (!progressViewController) {
        progressViewController = [self.storyboard instantiateViewControllerWithIdentifier:[QWProgressViewController identifier]];
    }
    return progressViewController;
}

- (void)showProgressView
{
    [self addChildViewController:[self progressViewController]];
    [self progressViewController].view.frame = self.view.bounds;
    [self.view addSubview:[self progressViewController].view];
}

- (void)hideProgressView
{
    if ([self progressViewController].parentViewController == self) {
        [[self progressViewController] removeFromParentViewController];
        [[self progressViewController].view removeFromSuperview];
    }
}

@end
