//
//  QWSelectCityViewController.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWSelectCityViewController.h"
#import "QWWeatherDetailsViewController.h"
#import "QWService.h"

static NSString *kWeatherDetailsSegueIdentifier = @"WeatherDeteils";


@interface QWSelectCityViewController ()

@end

@implementation QWSelectCityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self onCityNameChanged:self.cityNameTextField];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.cityNameTextField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.cityNameTextField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self onClickDone:textField];
    return YES;
}

#pragma mark - Actions
- (IBAction)onClickDone:(id)sender
{
    [self showProgressView];
    QWService *service = [QWService sharedInstance];
    QWWeatherRequest *request = [service requestByCityName:self.cityNameTextField.text];
    __block BOOL isMoved = false;
    [service getWeather:request withHandler:^(QWWeatherData *wetherData, NSError *error, QWWeatherDataState state) {
        [self hideProgressView];
        if (error) {
            [[UIAlertView alertViewWithError:error] show];
        } else {
            if (!isMoved) {
                [self performSegueWithIdentifier:kWeatherDetailsSegueIdentifier sender:request];
                isMoved = true;
            }
        }
        
    }];
}

- (IBAction)onCityNameChanged:(id)sender
{
    self.doneButton.enabled = (self.cityNameTextField.text.length >= 2);
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kWeatherDetailsSegueIdentifier]) {
        QWWeatherDetailsViewController *vc = segue.destinationViewController;
        vc.weatherRequest = sender;
    }
}


@end
