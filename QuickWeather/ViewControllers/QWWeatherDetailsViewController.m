//
//  QWWeatherDetailsViewController.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWWeatherDetailsViewController.h"
#import "QWWeatherData.h"
#import "QWWeatherRequest.h"
#import "QWWeatherChartViewController.h"
#import "QWService.h"

@interface QWWeatherDetailsViewController ()

@end

@implementation QWWeatherDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applyWeatherData)
                                                 name:kNotificationWeatherLoaded
                                               object:nil];
    [self applyWeatherData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setWeatherRequest:(QWWeatherRequest *)weatherRequest
{
    _weatherRequest = weatherRequest;
    [self applyWeatherData];
}

- (void)applyWeatherData
{
    if ([self isViewLoaded]) {
        self.cityNameLabel.text = self.weatherRequest.weatherData.location;
        self.temperatureLabel.text = [NSString stringWithFormat:@"%ld°C",(long)self.weatherRequest.weatherData.temperature.integerValue];
        self.weatherIconImageView.image = self.weatherRequest.weatherData.weatherIconImage;
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ForecastChart"]) {
        QWWeatherChartViewController *weatherChartViewController = segue.destinationViewController;
        weatherChartViewController.weatherRequest = self.weatherRequest;
    }
}


@end
