//
//  UIViewController+ProgressView.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 25.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ProgressView)

- (void)showProgressView;
- (void)hideProgressView;

@end
