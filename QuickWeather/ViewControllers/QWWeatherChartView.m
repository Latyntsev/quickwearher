//
//  QWWeatherChartView.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWWeatherChartView.h"

@implementation QWWeatherChartView

- (void)drawRect:(CGRect)rect
{
    UIColor *chartColor = [UIColor blackColor];
    UIColor *ordinateColor = [UIColor lightGrayColor];
    UIColor *textColor = [UIColor redColor];
    UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    
    
    
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 20, 20, 20);
    CGRect frame = UIEdgeInsetsInsetRect(self.bounds,insets);
    CGFloat x = frame.origin.x;
    CGFloat y = frame.origin.y;
    CGFloat width = frame.size.width;
    CGFloat height = frame.size.height;
    
    [super drawRect:rect];
    
    [chartColor setStroke];
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    
    for (NSInteger i = 0; i < [self numberOfPoints]; i++ ) {
        
        CGPoint point = [self pointAtIndex:i];
        point = (CGPoint){point.x * width + x, point.y * height + y};
        if (i == 0) {
            [bezierPath moveToPoint:point];
        } else {
            [bezierPath addLineToPoint:point];
        }
    }
    [bezierPath stroke];
    
    
    
    UIBezierPath *ordinatesPath = [UIBezierPath bezierPath];
    [ordinateColor setStroke];
    [ordinatesPath moveToPoint:(CGPoint){0,height + y}];
    [ordinatesPath addLineToPoint:(CGPoint){width + x,height + y}];
    
    [ordinatesPath moveToPoint:(CGPoint){x,0}];
    [ordinatesPath addLineToPoint:(CGPoint){x,self.frame.size.height}];
    
    [ordinatesPath moveToPoint:(CGPoint){width + x,0}];
    [ordinatesPath addLineToPoint:(CGPoint){width + x,height + y}];
    [ordinatesPath stroke];
    
    NSString *minXLabel = [self labelByType:QWWeatherChartViewLabelType_minX];
    NSString *maxXLabel = [self labelByType:QWWeatherChartViewLabelType_maxX];
    NSString *minYLabel = [self labelByType:QWWeatherChartViewLabelType_minY];
    NSString *maxYLabel = [self labelByType:QWWeatherChartViewLabelType_maxY];
    

    
    NSDictionary *stringAttributes = @{NSFontAttributeName:font,
                                       NSForegroundColorAttributeName:textColor};
    
    
    CGFloat labelOffset = 2;
    [maxXLabel drawAtPoint:(CGPoint){x + labelOffset,y} withAttributes:stringAttributes];
    [minXLabel drawAtPoint:(CGPoint){x + labelOffset,height + y - font.pointSize - labelOffset} withAttributes:stringAttributes];
    
    
    [minYLabel drawAtPoint:(CGPoint){x + labelOffset,height + y} withAttributes:stringAttributes];
    [maxYLabel drawAtPoint:(CGPoint){width + x - [minYLabel sizeWithAttributes:stringAttributes].width - labelOffset,height + y} withAttributes:stringAttributes];
}

- (void)reloadData
{
    [self setNeedsDisplay];
}

#pragma mark - Data Source Wrapper
- (NSInteger)numberOfPoints
{
    return [self.dataSource weatherChartViewNumberOfPoints:self];
}

- (CGPoint)pointAtIndex:(NSInteger)index
{
    CGPoint point = [self.dataSource weatherChartView:self pointAtIndex:index];
    NSAssert(point.x <= 1 && point.x >= 0, @"point x should be between 0..1");
    NSAssert(point.y <= 1 && point.y >= 0, @"point y should be between 0..1");
    return point;
}

- (NSString *)labelByType:(QWWeatherChartViewLabelType)labelType
{
    return [self.dataSource weatherChartView:self labelAtType:labelType];
}



@end

