//
//  QWService.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWService.h"
#import "QWWeatherRequestByCityName.h"
#import "QWWeatherRequestByCoordinate.h"
#import "QWOpenWeatherMapService.h"
#import "QWWeatherData.h"
#import "QWWeatherForecastData.h"


NSString * const kNotificationWeatherLoaded = @"kNotificationWeatherLoaded";
NSString * const kNotificationForecastLoaded = @"kNotificationForecastLoaded";


@import CoreData;

@interface QWService()

@property (nonatomic,strong) id<QWWeatherDataSourceProtocol> weatherDataSource;
@property (nonatomic,strong) QWDataController *dataController;


@end

@implementation QWService


- (instancetype)initWithWetherData:(id<QWWeatherDataSourceProtocol>)weatherData andDataController:(QWDataController *)dataController
{
	self = [super init];
	if (self) {
		self.weatherDataSource = weatherData;
        self.dataController = dataController;
	}
	return self;
}

+ (instancetype)sharedInstance
{
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        QWOpenWeatherMapService *weatherDataSource = [[QWOpenWeatherMapService alloc] init];
        QWDataController *dataController = [[QWDataController alloc] init];
        instance = [[self alloc] initWithWetherData:weatherDataSource andDataController:dataController];
    });
    return instance;
}


- (QWWeatherRequest *)requestByCityName:(NSString *)cityName
{
    Class class = [QWWeatherRequestByCityName class];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityName = %@",cityName];
    QWWeatherRequestByCityName *request = [self.dataController getObjectByClass:class andPredicate:predicate];
    
    if (request) {

    } else {
        request = [self.dataController instanceOfModelClass:class];
        request.cityName = cityName;
    }
    
    return request;
}

- (QWWeatherRequest *)requestByCoordinate:(CLLocationCoordinate2D)coordinate
{
    Class class = [QWWeatherRequestByCoordinate class];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"latitud = %ld && longitude = %ld",coordinate.latitude,coordinate.longitude];
    QWWeatherRequestByCoordinate *request = [self.dataController getObjectByClass:class andPredicate:predicate];
    
    if (request) {

    } else {
        request = [self.dataController instanceOfModelClass:class];
        request.longitude = coordinate.longitude;
        request.latitud = coordinate.latitude;
    }
    
    return request;
}

#pragma mark - QWWeatherDataSourceProtocol
- (void)getWeather:(QWWeatherRequest *)weatherRequest withHandler:(GetWetherDataHandler)handler
{
    GetWetherDataHandler theHandler = [handler copy];
    
    if (weatherRequest.weatherData) {
        NSLog(@"Cache used");
        theHandler(weatherRequest.weatherData,nil,QWWeatherDataState_cache);
    }
    [self.weatherDataSource getWeather:weatherRequest withHandler:^(QWWeatherData *wetherData, NSError *error, QWWeatherDataState state) {
        
        if (!error) {
            weatherRequest.weatherData = wetherData;
            [self.dataController saveData];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationWeatherLoaded
                                                                object:wetherData];
        }
        
        theHandler(wetherData,error,state);
    }];
}

- (void)getWeatherForecast:(QWWeatherRequest *)weatherRequest
                   forDays:(NSInteger)dayCount
               withHandler:(GetWetherForeastDataHandler)handler
{
    GetWetherForeastDataHandler theHandler = [handler copy];
    
    if (weatherRequest.forecastData) {
        NSLog(@"Cache used");
        theHandler(weatherRequest.forecastData,nil,QWWeatherDataState_cache);
    }
    [self.weatherDataSource getWeatherForecast:weatherRequest forDays:dayCount withHandler:^(QWWeatherForecastData *wetherForecastData, NSError *error, QWWeatherDataState state) {
        
        if (!error) {
            weatherRequest.forecastData = wetherForecastData;
            [self.dataController saveData];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationForecastLoaded
                                                                object:wetherForecastData];
        }
        
        theHandler(wetherForecastData,error,state);
    }];
}

- (QWWeatherData *)createWeatherDataObject
{
    return [self.dataController instanceOfModelClass:[QWWeatherData class]];
}

- (QWWeatherForecastData *)createWeatherForecastDataObject
{
    return [self.dataController instanceOfModelClass:[QWWeatherForecastData class]];
}


@end
