//
//  QWDataController.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

@import Foundation;

@interface QWDataController : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (id)getObjectByClass:(Class)class andPredicate:(NSPredicate *)predicate;
- (id)instanceOfModelClass:(Class)class;
- (void)saveData;

@end
