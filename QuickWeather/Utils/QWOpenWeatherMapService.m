//
//  QWOpenWeatherMapService.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWOpenWeatherMapService.h"
#import "QWOpenWeatherMapService+Model.h"
#import "QWService.h"


@implementation QWOpenWeatherMapService

- (NSString *)serviceLink
{
    static NSString *_serviceLink = @"http://api.openweathermap.org/data/2.5/";
    return _serviceLink;
}

- (NSURLSessionConfiguration *)sessionConfiguration
{
    return [NSURLSessionConfiguration defaultSessionConfiguration];
}

- (NSURLSession *)session
{
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[self sessionConfiguration]];
    return session;
}

- (NSURLRequest *)requestForResource:(NSString *)resource GetParams:(NSDictionary *)getParams
{
    NSString *params = @"";
    if (getParams) {
        for (NSString *key in getParams) {
            
            NSString *value = [getParams[key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSString *pair = [NSString stringWithFormat:@"%@=%@",key,value];

            if (params.length == 0) {
                params = [params stringByAppendingFormat:@"%@",pair];
            } else {
                params = [params stringByAppendingFormat:@"&%@",pair];
            }
        }
    }
    
    NSString *urlString = [self.serviceLink stringByAppendingFormat:@"%@",resource];
    if (params.length > 0) {
        urlString = [urlString stringByAppendingFormat:@"?%@",params];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    return [NSURLRequest requestWithURL:url];
}

- (id)parseData:(NSData *)data error:(NSError *__autoreleasing *)error
{
    return [NSJSONSerialization JSONObjectWithData:data options:0 error:error];
}

- (void)getWeather:(QWWeatherRequest *)weatherRequest withHandler:(GetWetherDataHandler)handler;
{
    NSAssert(weatherRequest, @"weatherRequest can't be empty");
    NSURLRequest *request = [self requestForResource:@"weather" GetParams:[weatherRequest openWeatherMapParams]];
    NSURLSessionDataTask *task = [[self session] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        id parsedData = nil;
        if (!error) {
            parsedData = [self parseData:data error:&error];
            if (parsedData && !error) {
                NSInteger code = [parsedData[@"cod"] integerValue];
                NSString *message = parsedData[@"message"];
                if (code != 200) {
                    error = [NSError errorWithDomain:@"com.latyntsev.quick_weather"
                                                code:code
                                            userInfo:@{NSLocalizedDescriptionKey:message}];
                }
            }
        }
        
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            QWWeatherDataState state = QWWeatherDataState_loaded;
            QWWeatherData *weatherData = nil;
            if (error) {
                state = QWWeatherDataState_error;
            } else {
                if (parsedData) {
                    weatherData = [[QWService sharedInstance] createWeatherDataObject];
                    [weatherData fillOpenWeatherMapData:parsedData];
                }
            }
            handler(weatherData,error,state);
        }];
        
    }];
    
    [task resume];
}

- (void)getWeatherForecast:(QWWeatherRequest *)weatherRequest forDays:(NSInteger)dayCount withHandler:(GetWetherForeastDataHandler)handler
{
    NSAssert(weatherRequest, @"weatherRequest can't be empty");
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"cnt":@(dayCount).stringValue}];
    [params addEntriesFromDictionary:[weatherRequest openWeatherMapParams]];
    NSURLRequest *request = [self requestForResource:@"forecast/dayly" GetParams:params];
    NSURLSessionDataTask *task = [[self session] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        id parsedData = nil;
        if (!error) {
            parsedData = [self parseData:data error:&error];
            if (parsedData && !error) {
                NSInteger code = [parsedData[@"cod"] integerValue];
                NSString *message = parsedData[@"message"];
                if (code != 200) {
                    error = [NSError errorWithDomain:@"com.latyntsev.quick_weather"
                                                code:code
                                            userInfo:@{NSLocalizedDescriptionKey:message}];
                }
            }
        }
        
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            QWWeatherForecastData *weatherForecastData = nil;
            QWWeatherDataState state = QWWeatherDataState_loaded;
            if (error) {
                state = QWWeatherDataState_error;
            } else {
                if (parsedData) {
                    weatherForecastData = [[QWService sharedInstance] createWeatherForecastDataObject];
                    [weatherForecastData fillOpenWeatherMapData:parsedData];
                }
            }
            
            handler(weatherForecastData,error,state);
        }];
        
    }];
    
    [task resume];
}

@end
