//
//  UIAlertView+NSError.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (NSError)

+ (instancetype)alertViewWithError:(NSError *)error;

@end
