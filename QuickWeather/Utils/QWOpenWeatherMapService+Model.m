//
//  QWOpenWeatherMapService+Model.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWOpenWeatherMapService+Model.h"
#import "QWWeatherRequestByCityName.h"
#import "QWWeatherRequestByCoordinate.h"
#import "QWService.h"

@implementation QWWeatherRequestByCityName(QWOpenWeatherMapService)

- (NSDictionary *)openWeatherMapParams
{
    return @{@"q":self.cityName};
}

@end


@implementation QWWeatherRequestByCoordinate(QWOpenWeatherMapService)

- (NSDictionary *)openWeatherMapParams
{
    return @{@"lat":[NSString stringWithFormat:@"%lf",self.latitud],
             @"lon":[NSString stringWithFormat:@"%lf",self.longitude]};
}

@end

@implementation QWWeatherData(QWOpenWeatherMapService)

- (void)fillOpenWeatherMapData:(NSDictionary *)data
{
    if (data) {
        CGFloat temperature = [[data valueForKeyPath:@"main.temp"] doubleValue];
        //Convert Kelvin to Celsius
        self.temperature = @(temperature - 273.15);
        
        self.weatherType = QWWeatherType_clear;
        NSString *weather = [data valueForKeyPath:@"weather.main"][0];
        if ([@[@"Clouds",@"Thunderstorm"] containsObject:weather]) {
            self.weatherType = QWWeatherType_clouds;
        }
        
        if ([@[@"Drizzle",@"Rain",@"Snow"] containsObject:weather]) {
            self.weatherType = QWWeatherType_rain;
        }
        NSString *country = [data valueForKeyPath:@"sys.country"];
        self.location = [data valueForKeyPath:@"name"];
        if (country) {
            self.location = [self.location stringByAppendingFormat:@",%@",country];
        }
        
        NSTimeInterval timeInterval = [data[@"dt"] doubleValue];
        self.date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    }
}

@end

@implementation QWWeatherForecastData(QWOpenWeatherMapService)

- (void)fillOpenWeatherMapData:(NSDictionary *)data
{
    if (data) {
        NSArray *dataList = data[@"list"];
        for (NSDictionary *dataListItem in dataList) {
            QWWeatherData *forecastDataItem = [[QWService sharedInstance] createWeatherDataObject];
            [forecastDataItem fillOpenWeatherMapData:dataListItem];
            forecastDataItem.forecast = self;
        }
    }
}

@end