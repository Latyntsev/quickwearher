//
//  QWLocationService.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 25.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreLocation;

@interface QWLocationService : NSObject

typedef void(^GetLocationHandler)(CLLocationCoordinate2D coordinate, NSError *error);

- (void)getCurrentLocation:(GetLocationHandler)handler;

@end
