//
//  QWWeatherDataSourceProtocol.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;

@class QWWeatherData;
@class QWWeatherRequest;
@class QWWeatherForecastData;


typedef NS_ENUM(NSInteger, QWWeatherDataState) {
	QWWeatherDataState_loaded,
	QWWeatherDataState_cache,
	QWWeatherDataState_error
};

typedef void(^GetWetherDataHandler)(QWWeatherData *wetherData, NSError *error, QWWeatherDataState state);
typedef void(^GetWetherForeastDataHandler)(QWWeatherForecastData *wetherForecastData, NSError *error, QWWeatherDataState state);


@protocol QWWeatherDataSourceProtocol <NSObject>

- (void)getWeather:(QWWeatherRequest *)weatherRequest withHandler:(GetWetherDataHandler)handler;
- (void)getWeatherForecast:(QWWeatherRequest *)weatherRequest
                   forDays:(NSInteger)dayCount
               withHandler:(GetWetherForeastDataHandler)handler;

@end
