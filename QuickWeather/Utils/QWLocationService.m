//
//  QWLocationService.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 25.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWLocationService.h"

@interface QWLocationService() <CLLocationManagerDelegate>

@property (nonatomic,copy) GetLocationHandler handler;
@property (nonatomic,strong) CLLocationManager *locationManager;
@end

@implementation QWLocationService

- (void)getCurrentLocation:(GetLocationHandler)handler
{
    self.handler = handler;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    [self sendResult:location.coordinate andError:nil];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [self sendResult:kCLLocationCoordinate2DInvalid andError:error];
}

- (void)sendResult:(CLLocationCoordinate2D)coordinate andError:(NSError *)error
{
    if (self.handler) {
        self.handler(coordinate,error);
        self.handler = nil;
        [self.locationManager stopUpdatingLocation];
        self.locationManager = nil;
    }
}
@end
