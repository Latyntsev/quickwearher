//
//  QWOpenWeatherMapService+Model.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "QWOpenWeatherMapService.h"
#import "QWWeatherRequest.h"
#import "QWWeatherForecastData.h"
#import "QWWeatherData.h"

@interface QWWeatherRequest(QWOpenWeatherMapService)

- (NSDictionary *)openWeatherMapParams;

@end


@interface QWWeatherData(QWOpenWeatherMapService)

- (void)fillOpenWeatherMapData:(NSDictionary *)data;

@end


@interface QWWeatherForecastData(QWOpenWeatherMapService)

- (void)fillOpenWeatherMapData:(NSDictionary *)data;

@end