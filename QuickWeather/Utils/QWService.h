//
//  QWService.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

@import Foundation;
#import "QWWeatherDataProtocol.h"
#import "QWWeatherRequest.h"
#import "QWDataController.h"

extern  NSString *const kNotificationWeatherLoaded;
extern  NSString *const kNotificationForecastLoaded;

@interface QWService : NSObject <QWWeatherDataSourceProtocol>

+ (instancetype)sharedInstance;
- (instancetype)initWithWetherData:(id<QWWeatherDataSourceProtocol>)weatherData andDataController:(QWDataController *)dataController;


- (QWWeatherRequest *)requestByCityName:(NSString *)cityName;
- (QWWeatherRequest *)requestByCoordinate:(CLLocationCoordinate2D)coordinate;

- (QWWeatherData *)createWeatherDataObject;
- (QWWeatherForecastData *)createWeatherForecastDataObject;

@end
