//
//  QWOpenWeatherMapService.h
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 7/24/14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QWWeatherDataProtocol.h"

@interface QWOpenWeatherMapService : NSObject<QWWeatherDataSourceProtocol>

@end
