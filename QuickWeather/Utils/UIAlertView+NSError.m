//
//  UIAlertView+NSError.m
//  QuickWeather
//
//  Created by Aleksandr Latyntsev on 26.07.14.
//  Copyright (c) 2014 latyntsev. All rights reserved.
//

#import "UIAlertView+NSError.h"

@implementation UIAlertView (NSError)

+ (instancetype)alertViewWithError:(NSError *)error
{
    return [[UIAlertView alloc] initWithTitle:nil
                                      message:error.localizedDescription
                                     delegate:nil
                            cancelButtonTitle:@"Ok"
                            otherButtonTitles:nil];
}

@end
